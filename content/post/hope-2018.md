+++
description = "Anarchist Hackers at HOPE 2018"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers", "hope", "hispagatos"]
title= "Hope 2018"
date= 2018-07-27T13:44:08-07:00
images = [
  "https://fotos.hispagatos.org/_data/i/upload/2018/07/25/20180725162748-231e6161-me.jpg"
] # overrides the site-wide open graph image
+++

##### Hispagatos and other Anarchist Hackers at HOPE XII 2018

Well well well... This year Hope was very interesting for many things  

- First Anarchist Hacker Village in a US hackerCon
- Alt-Right, Fascist at the conference
- Very interesting talks like usual but this time, seemed more old school hacker ethics, Anarchism, social justice types  

First of all, We will want to thank [Mitch Altman](https://en.wikipedia.org/wiki/Mitch_Altman) from the [Anarchist hacker space](https://gigaom.com/2014/08/01/welcome-back-anarchist-hackers-noisebridge-reopens-after-cleaning-house/) [Noisebridge](https://www.noisebridge.net). Also all our comrades and friends from all involved collectives and individuals that made this possible and helped during the conference at the village.

Here is my personal opinion, I been in the hacking culture world since the late 80's and I seen the hacker culture take a bad twist into comercial, corporate, centralized, consumerism and capitalist world, **But** in the last years, there has been a world wide push to bring back the original hacker ethics and the anarchist ideas back into it and this is very much reflected in old hacker conferences like [CCC](https://en.wikipedia.org/wiki/Chaos_Communication_Congress) and [HOPE](https://hope.net/) and I feel good in the direction we going, where before I was very pessimistic that the Information security mafia/complex was eating all of our beloved culture and destroying our ethical ideas for money and fame.

In a negative note, also the capitalist, the right wing libertarians and the system overloards are also counter attacking and not just in hacking but in society in general, we had our share of issues at the HOPE confere as well.. but I will let you guys make your own opinion on this since it has been documented and we [signed a collective statement](https://medium.com/@nofashathope/statement-no-fascists-at-hope-c6873a64cc94) and we are preparing another one to counter attack the fake news and rumours sorronding all things that came to happen.

here are some links for your RTFM:

- https://medium.com/@nofashathope/statement-no-fascists-at-hope-c6873a64cc94
- https://motherboard.vice.com/amp/en_us/article/wjkjzm/what-went-wrong-at-the-hope-hacking-conference?utm_source=reddit.com
- https://boingboing.net/2018/07/26/nazis-vs-hope.html
- https://c4ss.org/content/51088

Do not take us in the wrong way, we love HOPE, we think as I mention above one of the oldest hacker conferences and one that has keep hacker culture alive, but we are anarchist hackers, when we see fascists making their move, we fight back, we have to make sure HOPE organizers do not just write beautiful CoC's - code of conduct's, but also enforce them so we can keep having a harassment/fascist free zone where we can walk with out getting stab in the back or a beat down.  

Regarding the Hacker Anarchist Village, we though it was going to be a small area, but turned out to be the biggest village of the conference! we are very happy, we had fliers, T-shirts, patches, flags, banners, posters, we run out of fliers and posters and patches many times and we had to run for more, we were REALLY happy about our conversations, but who are we kidding.. is a hacker conference most hackers are anarchists so was not that unsurprisely unexpected.   


* We will be in much more details speaking about this and more at our next Anarchist Hacker Show [**Hackernol**](https://video.hispagatos.org/accounts/hackernol/videos).  
* We will also update the [Foto albun](https://fotos.hispagatos.org/index.php?/category/23) with pics as we get them,


ReK2  
Hispagatos  
a(A)a  

