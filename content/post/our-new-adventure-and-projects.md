+++
date = "2016-04-04T22:24:57-07:00"
draft = false
title = "Our new adventure and projects"

+++

<p>Hello everyone! I'm super happy to announce an array off changes to <b>hispagatos</b>, new members, new projects, new site! now based on <a href="https://gohugo.io/"> Hugo</a>  a
static web site builder under golang!(that I been falling in love more and more)
and after some other minor stuff I myself started a security company, is been a <a href="2013-10-10-a-bit-of-our-history.markdown">long way</a></p>

<p>new members:<br>
<ul>
<li>m11es</li>
<li>Thibaud</li>
<li>Trizz(was a member before back in the day but we recently got connected again)</li>
</ul>


<p>Another very cool interesting project we have.. I always been interested and involved with malware analysis and like any
curious person I like to know what OS's and devices are infected and by what.. so honeypots are the solution for that, I do not
believe honeypots are a solution for all solutions but I do like the flexibelity it gives you... I can monitor in the wild  what script kiddies, bots, malware and other types of attacks, I can find out for example that there are still windows XP computers out there.. and I can peek into stats  and see
what areas they are located at.. and much more ... ;) for a curious person this is great..
here is our <a href="https://pot.hispagatos.org:8443"> LIVE ATTACK MAP</a>  based on <a href="https://www.honeynet.org/">the honeymap project</a> and more directly involved with <a href="https://github.com/threatstream/mhn">The Moderm Honey Network</a> that we use, and we have added some other features to it.</p>
<p> I personally are moderating <a href="https://www.reddit.com/r/HackBloc/">HackBloc"</a> on reddit and some other interesting groups, come join us!</a>

<p>And for the last part, I been doing freelancing here and there, mostly during 2012-2014 it was fun! very scary, no healthcare, and lots of taxes.. 
but I really enjoyed it, so I recently decided to go LLC so started <a href="https://www.stealthy-cybersecurity.com">Stealthy-CyberSecurity LLC</a>  the site is not yet even 15% done, is mostly fillings as of April 4th.</p>

<p>Another project we trying to init, is the <a href="https://noisebridge.net/wiki/DC415">DefCon415</a> group, and you can chat with us at <a href="https://chat.binaryfreedom.info/dc415">chat.binaryfreedom.info</a> ask for an invite, this is still a long shot, mostly because it does not depends only on us but other people to help and contribute... but I will be updating the blog more from now on.</p>

<p>and for the very last point, I want to salute my good friends Pedro, from Alicante. Josh, from Boston, Luis from Boston I miss you guys and all the real underground hacking scene out there and the real <a href="https://anarcho-hacker.info">anarcho-hackers and cyberpunks</a></p>

<br>
<a href="https://keybase.io/cfernandez">Fernandez Christian</a>. aka <a href="https://noisebridge.net/wiki/User:Rek2">ReK2/ReK2WildS/ReK2GnuLInux</a>  <a href="https://hackstory.net/BBK">BBK</a>,Hispagatos,<a href="https://binaryfreedom.info">Binaryfreedom</a>,.....?

