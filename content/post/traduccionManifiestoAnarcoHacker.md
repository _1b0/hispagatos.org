+++
description = "Translation of the anarchohacker-manifesto to spanish"
draft = false
toc = false
categories = ["technology", "anarchism", "hacker"]
tags = ["tech", "hacking", "hackers", "anarchism", "translation"]
title= "Traduccion del Manifiesto Anarco-Hacker"
date= 2019-08-08T17:41:08+02:00
images = [
  "https://theanarchistlibrary.org/library/a-m-anarchohacker-manifesto-2018-2.png"
] # overrides the site-wide open graph image
+++

# El Manifiesto Anarco Hacker (2018) v.2
### Somos hackers, somos anarquistas, somos curiosos.

El siguiente texto es una traducción del Manifiesto Anarco Hacker. Podeis encontrar la versión original aquí: 
https://theanarchistlibrary.org/library/anarchohacker-manifesto-2018

----

<img src="https://theanarchistlibrary.org/library/a-m-anarchohacker-manifesto-2018-2.png"/><br/>

----

**N**os gusta conocer como funcionan las cosas. Nos gusta inventar, modificar y mejorar la tecnología como a cualquier otro hacker. Lo hacemos sin ánimo de lucro. Como hackers lo hacemos por el conocimiento y para satisfacer nuestra curiosidad, y hemos decidido utilizar esto como medio para crear un mundo mejor. La mayor parte de nosotros eramos rebeldes en la escuela, y probablemente nos metiéramos en problemas más de una vez. Crecimos y vimos al sistema como nuestro enemigo. Eramos bienvenidos en grupos de hackers pero en muchos casos tenían una limitada visión política y seguían repitiendo la misma propaganda del sistema de siempre. El resto de hackers que conocíamos se pensaban que estábamos locos cuando nos uníamos a las protestas en las calles y cuando leíamos a Bakunin, de Cleyre, Marsh y otros teóricos políticos.

Mientras nosotros estábamos en las revueltas de Seattle, Genova, Berlin, Atenas y Toronto aprendiendo quien es el verdadero enemigo de los derechos y libertades, nos dimos cuenta de que no es solo el gobierno, sino el capitalismo, la religión, la raza, el género, el nacionalismo, el imperialismo, el patriarcado, etc. Ellos son nuestro enemigo. Con el tiempo nos convertimos en los primeros hacktivistas y formamos colectivos hacker con valores anarquistas como actuación consensuada, democracia directa, organización horizontal, colaboración masiva y modelos de decisión federada y/o descentralizada. Escribimos herramientas de seguridad, guías, tormentas de datos con el objetivo de ayudar a los disidentes alrededor del mundo y a aquellos atrapados en las mismas estructuras y fronteras de los estados. Pensábamos que no era suficiente así que creamos frentes online de hacktivistas, algunos de los cuales siguen creciendo hoy en día, y participamos en sus acciones.  También observamos una falta de experiencia política en los recién llegados así que decidimos centrar esfuerzos en educación y en trabajar una mente crítica.

Estamos cansados de ser víctimas sin luchar, cansados de tener las habilidades y no darles un uso adecuado,  y para nosotros un buen uso no es trabajar para vosotros y vuestros perros. Ahora que hemos evolucionado somos la insurgencia. Nos tenéis miedo. Utilizáis vuestros estados y vuestros medios de comunicación para inventaros más propaganda y criminalizar a nuestros compañeros alrededor del mundo, mintiendo sobre los luchadores por la libertad, haciéndonos parecer el enemigo, impotente o perdido, pero ahora es diferente. Ahora somos legión. Nuestras barricadas no solo son electrónicas, y tener por seguro que tenemos más habilidad que cualquiera de vuestros perros  “buscafortunas” que habéis convencido para que trabajen a vuestro servicio. Incluso ahora, algunos de los nuestros trabajan para vosotros y contra vosotros.

**N**o enviamos información sobre nuestras acciones a vuestros medios de comunicación, las mantenemos para nosotros, sabéis quienes somos, sabéis que no estamos dispuestos a gobernar por celebridad, ni capital, y esto es lo que os asusta. Estamos decididos a derribar vuestro sistema ladrillo a ladrillo y byte a byte, desde dentro y desde fuera hemos tenido éxito hasta ahora. [All your bases are belong to us](https://es.wikipedia.org/wiki/All_your_base_are_belong_to_us). Y a todos aquellos que suplicáis por la libertad, liberaros vosotros mismos. Un perro que suplica nunca será libre de su amo.

La información debe ser libre pues aquellos que poseen la información y controlan su acceso tienen el poder. Por eso debemos liberarla y descentralizar ese poder.

Concebimos todas las formas de resistencia digital, y las usamos para mostrarles al mundo que podemos ganar todas estas batallas y más, por eso hemos decidido no separarnos de nuestros compañeros anarquistas con otras tácticas, sino juntarnos todos, no nos conformamos con un solo frente, los cubrimos todos. ¡Llevamos a la gente a la calle y traemos al mundo electrónico a activistas que no podrían haberlo hecho antes de aumentar nuestro número! ¡Diversidad de tácticas y diversidad de frentes!

En nuestra opinión todo individuo que, en cualquier estado actual, predica con la urna electoral, está perdiendo el tiempo y perpetuando un criminal estado capitalista. Preferimos contribuir en este nuevo mundo con un espíritu basado en ideales anarquistas y colaboración masiva. Para ello, la mayor parte de nosotros creemos en diferentes estrategias, no solo trabajar desde sombra de las redes más oscuras, sino también en las calles y comunidades con movimientos insurgentes de todo tipo.

**N**uestra insurgencia utiliza una diversa colección de acciones dependiendo de lo que cada uno de nosotros decida que es necesario y somos capaces de en ese mismo instante, trabajar juntos en lo que mejor se nos da.

**N**osotros somos idealistas,

**N**osotros escribimos,

**N**osotros amamos,

**N**osotros construimos,

**N**osotros creamos,

**N**osotros destruimos,

**N**osotros emancipamos,

**N**osotros compartimos y

**N**osotros luchamos contra la opresión en cualquiera de sus formas. Y por supuesto, Nosotros hackeamos.

<br/><br/>

a(A)a Anon Anarchist Action, YourAnonCentral, y otros miembros de Anonymous y otros colectivos que prefieren permanecer en el anonimato han participado en la creación de este manifiesto que en ningún caso, está finalizado.

Fuentes:
    1. https://theanarchistlibrary.org/library/anarchohacker-manifesto-2018
