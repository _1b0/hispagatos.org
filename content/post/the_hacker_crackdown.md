+++
description = "The Hacker Crackdown a(A)a fuck infosec"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "The_hacker_crackdown"
date= 2019-02-15T23:42:52-08:00
images = [
  "https://en.wikipedia.org/wiki/The_Hacker_Crackdown#/media/File:Hackcrackcov.jpg"
] # overrides the site-wide open graph image
+++

The Hacker Crackdown: un esfuerzo colaborativo de 18 meses del Servicio Secreto de los Estados Unidos y AT&T disuelto para tomar el control de Internet a principios de los años noventa. Se enfocan en lo que hace que Internet sea una herramienta tan poderosa: la cultura hacker. Los piratas informáticos eran personas que creían que el acceso a Internet y la tecnología informática deberían ser lo más libres y abiertos posible. Creían que el ciberespacio no era un lugar para que los gobiernos censuraran y vigilaran. No era un lugar para que las corporaciones pudieran controlar y manipular. Para los hackers, el ciberespacio era una nueva frontera que liberaría la mente y abriría infinitas oportunidades. The Hacker Crackdown fue la guerra sucia ideológica del gobierno de los Estados Unidos para tomar el control del reino digital. Esta es una historia de personas, para quienes el ciberespacio era una zona de guerra. Vigilancia gubernamental. La censura corporativa. Manipulación de los medios. Esta es la realidad de internet hoy. Poco a poco, las corporaciones multimillonarias están tomando el control de lo que podemos hacer, ver y decir sobre el ciberespacio. No podemos hacer comentarios en las redes sociales, chatear con personas o navegar en la web, sin que el gobierno en algún lugar tome una copia en una base de datos maliciosa debidamente pagada por nosotros. Es casi como si hubiera una guerra librando por este enorme espacio digital que alguna vez se soñó con liberar a las personas. Una frontera donde todo el mundo podría desatar su potencial. Una verdadera igualdad de oportunidades. Y parece que los gobiernos y las grandes corporaciones odian este empoderamiento de los pequeños. Lo desprecian tanto que trabajan mano a mano para atacar con toda su fuerza para negarnos cualquier expansión de los derechos constitucionales en el ámbito digital. Y todo comenzó en los albores de la Internet comercial con una drástica represión contra su más temida oposición: los hackers. La represión de los piratas informáticos de 1990 sentó un precedente para gran parte de lo que está sucediendo hoy. Hago estos videos porque creo que oponerse al poder y la autoridad ilegítima es un deber moral. Creo que todos los humanos son fundamentalmente libres. Pero esta libertad no se cuidará sola. Si usted también cree en esta causa y desea ayudar en esta búsqueda


English

---- 
The Hacker Crackdown - an 18-month long collaborative effort of the United States Secret Service and broken-up AT&T to take control of the Internet in the early 1990s. They stroke right at the heart of what makes the Internet such a powerful tool - the hacker culture. Hackers were people who believed access to the Internet and computer technology should be as free and open as possible. They believed cyberspace was a not place for governments to censor and monitor. It was not a place for corporations to control and manipulate. For hackers, cyberspace was a new frontier that would free the mind and open endless opportunities. The Hacker Crackdown was the United States government ideological dirty war to take control of the digital realm. This is a story of people, for whom cyberspace was a war zone. Government surveillance. Corporate censorship. Media manipulation. This is the reality of the Internet today. Bit by bit, multi-billion-dollar corporations are taking control of what we can do, see, and say on the cyberspace. We can’t make comments on social media, chat with people, or browse the web, without the government somewhere taking a copy in a rogue database dully paid for by us. It’s almost as if there was a waging war for this huge digital space that was once dreamed of as setting people free. A frontier where everybody could unleash their potential. A true equality of opportunities. And it seems governments and big corporations hate this empowerment of the little ones. They despise it so much they work hand-in-hand to attack with full force in order to deny us any expansion of Constitutional rights into the digital realm. And it all started at the dawn of the commercial Internet with a drastic crackdown on their most feared opposition: hackers. The Hacker Crackdown of 1990 set a precedent for much of what is happening today.

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://video.hispagatos.org/videos/embed/0cb32a37-2c54-4fbf-a9e0-d1691d92069d" frameborder="0" allowfullscreen></iframe>

Support hispagatos - what you need to do? donate? NOOOOOOOOOOOOOOOO

- just hack the system
- just fight for what you think is right
- just fight! DO SOMETHING!!!!

#hackthesystem

#antisec

AnonAnarchistAction

https://video.hispagatos.org

https://hispatatos.space

https://hispagatos.org
