+++
description = "Como entrar al nodo Hispagatos de matrix con Gomuks / How to join the Hispagatos matrix node  with Gomuks"
draft = false
toc = true
categories = ["technology"]
tags = ["tech", "encryption", "gomuks", "hispagatos"]
title = "Entrar al nodo Hispagatos de  Matrix con Gomuks / Join the Hispagatos Matrix node with Gomuks"
date = 2020-07-07T17:34:17+02:00
menu = "main"
+++

- [Español](#español)
- [English](#english)

# Español 

# Entrar al nodo Hispagatos de [Matrix](https://matrix.org/) con [Gomuks](https://matrix.org/docs/projects/client/gomuks)
Mini howto para instalar Gomuks y acceder a Hispagatos Matrix. Para más información, [RTFM](https://github.com/tulir/gomuks/wiki)

### Instalar

#### Gestor de paquetes (recomendado)

+ Arch Linux (AUR): [gomuks](https://aur.archlinux.org/packages/gomuks) and [gomuks-git](https://aur.archlinux.org/packages/gomuks-git)
+ NixOS: [gomuks](https://github.com/NixOS/nixpkgs/blob/master/pkgs/applications/networking/instant-messengers/gomuks/default.nix)
+ OpenSUSE (OBS): [home:albino:matrix/gomuks](https://build.opensuse.org/package/show/home:albino:matrix/gomuks)
+ Alpine Linux (testing): [gomuks](https://pkgs.alpinelinux.org/packages?name=gomuks&branch=edge)

#### Compilar del código fuente (solo si quiere ayudar con el desarrollo y sabe lo que está haciendo)

1. Instalar Go 1.13 o superior y ```libolm``` para encriptar los mensages.
2. Clone el repositorio: ```git clone https://github.com/tulir/gomuks.git```.
3. Entre en el directorio y haga ```go install``` para instalarlo en su __$GOPATH/bin__ o __$HOME/go/bin__ por defecto.

Ahora, para poder correrlo donde quieras, tendrás que añadir __$GOPATH/bin__ o __$HOME/go/bin__ a __$PATH__ en ```~/.profile``` para Bash o
```~/.zshenv``` para Zsh de la siguiente manera:
```
export PATH=...:$GOPATH/bin 
o 
export PATH=...:$HOME/go/bin
```


### Crear cuenta

Si no tienes una cuenta, sigue este [howto](https://hispagatos.org/post/howto_matrix-riot/) para crearla.


### Entrar a Hispagatos

```
Username: @<nombre_usuario-matrix>:hispagatos.org
Password: <contraseña-matrix>
Homeserver: https://matrix.hispagatos.org
```

Pulsa **login**, ve al canal de #hispagatos y preséntate con un
```
/rainbow iiiiiiiiiie
```

Happy Hacking. Deuses.



# English

# Join the Hispagatos [Matrix](https://matrix.org/) node with [Gomuks](https://matrix.org/docs/projects/client/gomuks)
Mini howto to install Gomuks and join in Hispagatos Matrix. For more information, [RTFM](https://github.com/tulir/gomuks/wiki)


### Install

#### Distribution package (recommended)

+ Arch Linux (AUR): [gomuks](https://aur.archlinux.org/packages/gomuks) and [gomuks-git](https://aur.archlinux.org/packages/gomuks-git)
+ NixOS: [gomuks](https://github.com/NixOS/nixpkgs/blob/master/pkgs/applications/networking/instant-messengers/gomuks/default.nix)
+ OpenSUSE (OBS): [home:albino:matrix/gomuks](https://build.opensuse.org/package/show/home:albino:matrix/gomuks)
+ Alpine Linux (testing): [gomuks](https://pkgs.alpinelinux.org/packages?name=gomuks&branch=edge)


#### Compiling from the source code (only for help with the development and you know what are you doing)

1. Install Go 1.13 or higher and ```libolm``` for encrypt the messages.
2. Clone the repo: ```git clone https://github.com/tulir/gomuks.git```.
3. Enter in the directory and do ```go install``` to install it in your __$GOPATH/bin__ or __$HOME/go/bin__ by default.

Now, to be able to run wherever you want, you will have to add your __$GOPATH/bin__ or __$HOME/go/bin__ to __$PATH__ in ```~/.profile``` 
for Bash or ```~/.zshenv``` for Zsh like this:
```
export PATH=...:$GOPATH/bin 
or 
export PATH=...:$HOME/go/bin
```


### Create account

If you don't have an account, follow this [howto](https://hispagatos.org/post/howto_matrix-riot/) (it is in spanish but it is easy to understand) to create it.


### Join in Hispagatos

```
Username: @<username-matrix>:hispagatos.org
Password: <password-matrix>
Homeserver: https://matrix.hispagatos.org
```

Press **login**, go to #hispagatos channel and introduce yourself with
```
/rainbow iiiiiiiiiie
```

Happy Hacking. Deuses.


# Links
## Hispagatos
---

- [Hispagatos blog](https://hispagatos.org)
- [Hispagatos hub](https://hubs.mozilla.com/zWXK8U6/hispagatos/)
- [Hispagatos lemmy](https://dev.lemmy.ml/c/hispagatos)
- [Hackerñol lbry](https://open.lbry.com/@Hackernol:7)
- [Hackerñol bittube](https://bittube.video/video-channels/hackernol/)
- [Hackstory](https://www.hackstory.net/BBK)
- [Rek2 mastodon](https://hispagatos.space/@rek2)
- [Rek2 keybase](https://keybase.io/rek2)
- [Rek2 bittube](https://bittube.video/accounts/rek2)
- [t1b0 mastodon](https://hispagatos.space/@_1b0)
- [t1b0 keybase](https://keybase.io/t1b0)
- [sigma0100 mastodon](https://hispagatos.space/@sigma4)
- [sigma0100 keybase](https://keybase.io/sigma0100)
- [MegageM mastodon](https://hispagatos.space/@MegxgeM)
- [MegageM keybase](https://keybase.io/mggm)
- [MegageM bittube](https://bittube.video/accounts/)
- [real-changeling mastodon](https://deadinsi.de/@forAll52)
- [real-changeling keybase](https://keybase.io/realchangeling)
- [Killab33z keybase](https://keybase.io/killab33z)
- [Killab33z mastodon](https://hispagatos.space/@Killab33z_OG)


## The movement
---

- [Anarcho Hackers Wiki ESP](https://www.noisebridge.net/wiki/Anarcohackers)
- [Anarcho Hackers Wiki](https://www.noisebridge.net/wiki/Anarchisthackers/)
- [Manifiesto Anarco Hacker ESP](https://hispagatos.org/post/traduccionmanifiestoanarcohacker/)
- [Anarcho Hackers Manifesto 2018](https://theanarchistlibrary.org/library/anarchohacker-manifesto-2018)
- [Anarcho\_hackers lemmy](https://dev.lemmy.ml/c/anarcho_hackers)
- [Hackbloc lemmy](https://dev.lemmy.ml/c/hackbloc)
- [La Ética Hacker ESP](https://hispagatos.org/post/la_etica_hacker/)
- [Guerilla Open Access Manifesto ESP](https://hispagatos.org/post/guerilla-open-access-manifesto/)
- [The Hacker Crackdown: Law and Disorder on the Electronic Frontier](https://en.wikipedia.org/wiki/The_Hacker_Crackdown)
- [Hackers: Heroes of the Computer Revolution](https://en.wikipedia.org/wiki/Hackers:_Heroes_of_the_Computer_Revolution)
- [Libertarian Socialism](https://en.wikipedia.org/wiki/Libertarian_socialism)
- [Anti-Authoriarianism](https://en.wikipedia.org/wiki/Anti-authoritarianism)
- [Anarcho-Communism](https://en.wikipedia.org/wiki/Anarcho-communism)

Sobre Hispagatos:  
Trabajamos duro para preservar la descentralización, seguridad y privacidad en el ciberespacio y motivar una sociedad tecno anarco comunista (STAC), horizontal y no jerarquizada donde la tecnología es hecha por personas para personas y no por corporaciones para controlar personas. a(A)a

About Hispagatos:  
We work hard to preserve decentralization,security and privacy in cyberspace and motivate towars an horizontal and non hierarchical techno-anarcho-communist society (TACS) where technology is made by people for the people not by corporate masters to control people. a(A)a
