+++
date = "2016-08-15T00:56:26-07:00"
draft = false
title = "hackernol"

+++

##### Yes finally our first episode is out

**I am happy to announce that our [first episode](https://www.youtube.com/watch?v=zGnAWfCUJjU) and [website](https://xn--hackerol-i3a.net/) are live and kicking!**

Do not expect a master piece, is our first podcast and we had lot of issues and we learn a lot.. and im am sure still more to learn :)

***Primer Episodio (versión chapuza) de Hackerñol.***
Especial sobre la prestigiosa conferencia hacker en NY llamada HOPE

##### Con entrevistas a:
* [Bernie S. de 2600](https://en.wikipedia.org/wiki/Bernie_S)
* [Captain Crunch](https://en.wikipedia.org/wiki/John_Draper)
##### Temas:
* [Hacktivismo](https://es.wikipedia.org/wiki/Hacktivismo)
* [free jeremy](hammond https://freejeremy.net/)
* [Free Julian Assange](https://www.freeassangenow.org/)
* [HackerSpaces](https://www.noisebridge.net/)

Desgraciadamente las entrevistas están en Inglés, pero estamos buscando alguien que nos haga la traducción. Y perdonar por la calidad tan baja, pero estamos empezando... poco a poco.. y no dudéis en dar consejos y demás ya que eso nos animará a seguir haciendo el programa cada dos semanas y mejorar la calidad

[![https://img.youtube.com/vi/zGnAWfCUJjU/0.jpg](https://img.youtube.com/vi/zGnAWfCUJjU/0.jpg)](https://www.youtube.com/watch?v=zGnAWfCUJjU)
<br>
Musica gracias a [DualCore](https://dualcoremusic.com/nerdcore)
Twitter de Dualcore: ***@dualcoremusic***
<br>
[Nuestra pagina web ***HACKERñOL***](https://xn--hackerol-i3a.net/)
##### Nuestras comunidades desde donde sale este programa:
* https://binaryfreedom.info
* https://hispagatos.org
* https://noisebridge.net/wiki/DC415

