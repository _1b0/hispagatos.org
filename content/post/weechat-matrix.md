+++
description = "Weechat for Matrix with OLM e2ee support"
draft = false
toc = true
categories = ["technology","matrix","hispagatos"]
tags = ["tech", "hacking", "hackers"]
title= "Weechat for Matrix with e2ee"
date= 2019-04-27T21:12:27-07:00
images = [
  "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Matrix_logo.svg/1200px-Matrix_logo.svg.png"
] # overrides the site-wide open graph image
+++



# WEECHAT FOR MATRIX WITH OLM e2ee support
+ and how to connect to hispagatos.org matrix server

<img src="/images/weechat-matrix-newsboat-taskwarrior.png" width="100%" height="100%">

---

## Create a matrix account on matrix.hispagatos.org

 * **[First we going to register using riot](https://about.riot.im/downloads)**
    - I suggest you use riot-desktop but riot-android should work just fine.
 * Open the matrix client, riot-desktop in our case but im sure other clients
  should work as well. Since this is focus on weechat we just going to sent
you to a preview howto in how to register an account for our hispagatos server.
 * To create an account follow [this tutorial](https://hispagatos.org/post/howto_matrix-riot/)
is in Spanish but you can use a online web translator and follow the images.

---

## install yay if you use Arch GNU/Linux
- Yet Another Yogurt - An AUR Helper Written in Go, GO is fast, much better than using scripting languages...
- common start using real programming languages :) python is for lamers such why anyone can script on it  ;)
  - if you already have a AUR package manager [use this](https://aur.archlinux.org/packages/yay/)
  - if you only use pacman then [install from github](https://github.com/Jguer/yay)

---
## Install weechat-matrix plugin

 + Get [weechat-matrix](https://matrix.org/docs/projects/client/weechat-matrix)
 + Follow the README.md instructions, this is not a howto for lamers so no need
to rewrite that is already wrote there :) so RTFM.
 + Autoload weechat-matrix plugin

 ``` 
 ln -sf ~/.weechat/python/matrix.py ~/.weechat/python/autoload/matrix.py
 ```

---

## Install cool fonts for console apps

- you have some options [here](https://wiki.archlinux.org/index.php/Fonts#Emoji_and_symbols)

```
 yay -S ttf-emojione-color
 yay -S noto-fonts-emoji
 yay -S ttf-twemoji-color
```
---

## Configure Weechat and connect

+ Set up weechat vault
    - You do NOT want to set your password in clear text in your config file instead we use the secure weechat option to encrypt our passwords just like any other password manager

 ```
 /secure passphrase <here your password to unlock the vault>
 /secure set hispagatospass <here your matrix.hispagatos.org pass>
 /save
 ```

+ Configure matrix server on weechat

 ```
 /matrix server add hispagatos_org matrix.hispagatos.org
 /set matrix.server.hispagatos_org.username rek2
 /set matrix.server.hispagatos_org.password "${sec.data.hispagatospass}"
 /save
 ```

+ Connect to the matrix server
 ```
 /matrix connect hispagatos_org
 ```

+ To import keys from another device,computer,client..
 ```
  /olm import /home/rek2/Descargas/riot-keys.txt <password of the backup>
 ```

+ To export keys to be backup or used in another device,computer,client..

 ```
 /olm export /home/rek2/Descargas/riot-keys.txt <password of the backup
 ```

+ Verify peoples devices,keys etc
  - you can verify more than one device, by using the domain only
 
 ```
 /olm verify @nick:hispagatos.org
 /olm verify :hispagatos.org
 /olm verify :matrix.org
 /olm verify @nick:matrix.org
 ```

+ Weechat matrix upload helper
  - this is where you put the plugin helper files this below is an example.
  - for Fish shell

      ```
      set -U fish_user_paths /home/rek2/herramientas/weechat-matrix/contrib $fish_user_paths
      ```
  - for Bash shell

    ```
    export PATH=$PATH:/home/rek2/herramientas/weechat-matrix/contrib
    ```

+ **Turn on auto connect** and check all weechat-matrix options and set them to your liking.
  - Scroll down to the option *.autoconnect change this to on, you turn it on by alt+space
 
 ```
 /fset matrix
 ```

+ Add [matrix_modes] to your weechat status bar
  - Hit alt+enter to edit. add [matrix_modes],  and/or  [matrix_typing_notice]
in the middle somewhere and hit <enter> again, I recomend removing some statuses that
you do not care about..

```
/fset bar.status.items
/save
```

+ Improve UI
  - if you want to select text press __shift__ or __alt__ depends on the terminal

```
 /set weechat.bar.input.size 0
 /set weechat.bar.input.size_max 2
 /mouse enable
 /mouse disable
```

## More plugins that will make quality of life better

+ https://weechat.org/scripts/source/urlgrab.py.html/

```
 /script load urlgrab.py
```

+ add it to autoload and RTFM
    - for [SWAY](https://swaywm.org/) change "xsel -i" for [wl-clipboard](https://github.com/bugaevc/wl-clipboard)

```
 /fset urlgrab
```

+ Spell check

```
 yay -S aspell-es
 yay -S aspell-en
 /set aspell.check.default_dict en,es
 /set aspell.check.suggestions 3
 /set aspell.color.suggestion *green
 /aspell enable`
 /save
```

+ Add the spell check suggestions to your weechat status bar, [aspell_dict],[aspell_suggest]

```
 /fset bar.status.items
```

## Make surf the default browser

+ look for netsurf.desktop and copy it

```
 yay -S surf
 sudo cp  netsurf.desktop surf.desktop
 sudo sed -i s/netsurf/surf/g surf.desktop
 xdg-settings set default-web-browser surf.desktop
```


## The End
+ Now this is just the tip of the iceberg, there are tons of weechat plugins, you can write your own, weechat is a hacker tool, this means is meant for hackers or people who likes to tinker and make things their own way and not scared of reading and learning and improving themself... for more things to learn start by reading the [weechat scripting and user guides](https://weechat.org/doc/)

- [Rek2](https://keybase.io/rek2)
- [weechat](https://weechat.org)
- [weechat-matrix](https://github.com/poljar/weechat-matrix)

<center>Happy Hacking</center>
